package com.sample;


public class VfcFluOrderLine {

    private String vfcFluOrderId;
    private String orderVersion;
    private String fluVaccineTypeId;
    private String orderQty;


    public String getVfcFluOrderId() {
        return vfcFluOrderId;
    }

    public void setVfcFluOrderId(String vfcFluOrderId) {
        this.vfcFluOrderId = vfcFluOrderId;
    }


    public String getOrderVersion() {
        return orderVersion;
    }

    public void setOrderVersion(String orderVersion) {
        this.orderVersion = orderVersion;
    }


    public String getFluVaccineTypeId() {
        return fluVaccineTypeId;
    }

    public void setFluVaccineTypeId(String fluVaccineTypeId) {
        this.fluVaccineTypeId = fluVaccineTypeId;
    }


    public String getOrderQty() {
        return orderQty;
    }

    public void setOrderQty(String orderQty) {
        this.orderQty = orderQty;
    }

}
