package gov.nyc.dohmh.cir.vsm.doh.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Table("vfc_flu_order")
public class VfcFluOrder {

    @Id
    @Column("vfc_flu_order_id")
    private String vfcFluOrderId;

    @Column("vfc_pin")
    private String vfcPin;

    @Column("flu_season_id")
    private String fluSeasonId;

    @Column("created_user_id")
    private String createdUserId;

    @Column("created_date_time")
    private LocalDateTime createdDatetime;

    @Column("last_mod_id")
    private String lastModId;

    @Column("last_mod_datetime")
    private LocalDateTime lastModDatetime;

    @PersistenceConstructor
    public VfcFluOrder(String vfcFluOrderId,
                       String vfcPin,
                       String fluSeasonId,
                       String createdUserId,
                       LocalDateTime createdDatetime,
                       String lastModId,
                       LocalDateTime lastModDatetime) {
        this.vfcFluOrderId = vfcFluOrderId;
        this.vfcPin = vfcPin;
        this.fluSeasonId = fluSeasonId;
        this.createdUserId = createdUserId;
        this.createdDatetime = createdDatetime;
        this.lastModId = lastModId;
        this.lastModDatetime = lastModDatetime;
    }

    public String getVfcFluOrderId() {
        return vfcFluOrderId;
    }

    public void setVfcFluOrderId(String vfcFluOrderId) {
        this.vfcFluOrderId = vfcFluOrderId;
    }


    public String getVfcPin() {
        return vfcPin;
    }

    public void setVfcPin(String vfcPin) {
        this.vfcPin = vfcPin;
    }


    public String getFluSeasonId() {
        return fluSeasonId;
    }

    public void setFluSeasonId(String fluSeasonId) {
        this.fluSeasonId = fluSeasonId;
    }


    public String getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

    public LocalDateTime getCreatedDatetime() {
        return createdDatetime;
    }

    public void setCreatedDatetime(LocalDateTime createdDatetime) {
        this.createdDatetime = createdDatetime;
    }


    public String getLastModId() {
        return lastModId;
    }

    public void setLastModId(String lastModId) {
        this.lastModId = lastModId;
    }


    public LocalDateTime getLastModDatetime() {
        return lastModDatetime;
    }

    public void setLastModDatetime(LocalDateTime lastModDatetime) {
        this.lastModDatetime = lastModDatetime;
    }

}
